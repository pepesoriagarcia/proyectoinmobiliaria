package com.pepe.housestore.retrofitService;

import android.content.Context;
import android.util.Log;

import com.pepe.housestore.Listener.DialogListener;
import com.pepe.housestore.Listener.Recargar;
import com.pepe.housestore.model.Propiedad;
import com.pepe.housestore.model.propiedad.MyPropertiesResponse;
import com.pepe.housestore.retrofit.Utils;
import com.pepe.housestore.retrofit.generator.ServiceGenerator;
import com.pepe.housestore.retrofit.generator.TipoAutenticacion;
import com.pepe.housestore.retrofit.service.MyPropertiesService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RetrofitMyProperties implements Recargar {
    private MyPropertiesService service;
    private Context ctx;
    private Recargar mListener;

    public RetrofitMyProperties(Context ctx){
        mListener = (Recargar) ctx;
        service = ServiceGenerator.createService(MyPropertiesService.class, Utils.getToken(ctx), TipoAutenticacion.JWT);
    }

    public void addProperti(Propiedad propiedad){
        Call<Propiedad> call = service.addProperti(propiedad);
        call.enqueue(new Callback<Propiedad>() {
            @Override
            public void onResponse(Call<Propiedad> call, Response<Propiedad> response) {
                if (response.isSuccessful()) {
                    //Snackbar.make(view,"Añadido a favorito", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    mListener.recargar();
                } else {
                    Log.e("RequestError", response.message());
                }
            }
            @Override
            public void onFailure(Call<Propiedad> call, Throwable t) {
                Log.e("RequestError", "onFailure");
            }
        });
    }
    public void deleteProperti(String id){
        Call<String> call = service.deleteProperti(id);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    //Snackbar.make(view,"Añadido a favorito", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    mListener.recargar();
                } else {
                    Log.e("RequestError", response.message());
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.e("RequestError", "onFailure");
            }
        });
    }
    public void editProperti(String id, Propiedad p){
        Call<Propiedad> call = service.editProperti(id, p);
        call.enqueue(new Callback<Propiedad>() {
            @Override
            public void onResponse(Call<Propiedad> call, Response<Propiedad> response) {
                if (response.isSuccessful()) {
                    mListener.recargar();
                } else {
                    Log.e("RequestError", response.message());
                }
            }
            @Override
            public void onFailure(Call<Propiedad> call, Throwable t) {
                Log.e("RequestError", "onFailure");
            }
        });
    }

    @Override
    public void recargar() {

    }
}