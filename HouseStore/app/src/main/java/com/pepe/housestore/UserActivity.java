package com.pepe.housestore;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.graphics.drawable.Drawable;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.pepe.housestore.Adapter.PropertiesRecyclerViewAdapter;
import com.pepe.housestore.model.ResponseContainer;
import com.pepe.housestore.model.meResponse;
import com.pepe.housestore.model.propiedad.PropertiesResponse;
import com.pepe.housestore.retrofit.Utils;
import com.pepe.housestore.retrofit.generator.ServiceGenerator;
import com.pepe.housestore.retrofit.generator.TipoAutenticacion;
import com.pepe.housestore.retrofit.service.PropertiesService;
import com.pepe.housestore.retrofit.service.UserService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserActivity extends AppCompatActivity {
    private meResponse user;
    private CollapsingToolbarLayout superior;
    private TextView email, name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        email = findViewById(R.id.email_u);
        name = findViewById(R.id.name_u);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.user);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        superior = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout_user);
        this.getUser();
    }

    public void getUser(){

        UserService service = ServiceGenerator.createService(UserService.class, Utils.getToken(UserActivity.this), TipoAutenticacion.JWT);
        Call<meResponse> call = service.me();

        call.enqueue(new Callback<meResponse>() {
            @Override
            public void onResponse(Call<meResponse> call, Response<meResponse> response) {
                if (response.isSuccessful()) {
                    user = response.body();
                    superior.setTitle(user.getEmail());
                    email.setText(user.getEmail());
                    name.setText(user.getName());
                } else {
                    Log.e("RequestError", response.message());
                }
            }
            @Override
            public void onFailure(Call<meResponse> call, Throwable t) {
                //View r = setContentView(R.layout.activity_main);
                Log.e("RequestError", "onFailure");
                //Snackbar.make(getActivity().findViewById(android.R.id.content),"Complete los campos", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }
        });
    }
}
