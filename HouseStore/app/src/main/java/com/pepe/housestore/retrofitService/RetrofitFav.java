package com.pepe.housestore.retrofitService;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.pepe.housestore.model.FavResponse;
import com.pepe.housestore.retrofit.Utils;
import com.pepe.housestore.retrofit.generator.ServiceGenerator;
import com.pepe.housestore.retrofit.generator.TipoAutenticacion;
import com.pepe.housestore.retrofit.service.FavService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RetrofitFav {

    private FavService service;

    public RetrofitFav(Context ctx){
        service = ServiceGenerator.createService(FavService.class, Utils.getToken(ctx), TipoAutenticacion.JWT);
    }


    public void addFav(String id){
        Call<FavResponse> call = service.addFav(id);
        call.enqueue(new Callback<FavResponse>() {
            @Override
            public void onResponse(Call<FavResponse> call, Response<FavResponse> response) {
                if (response.isSuccessful()) {
                    Log.e("add", response.message());
                    //Snackbar.make(view,"Añadido a favorito", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                } else {
                    Log.e("RequestError", response.message());
                }
            }
            @Override
            public void onFailure(Call<FavResponse> call, Throwable t) {
                Log.e("RequestError", "onFailure");
            }
        });
    }
    public void deleteFav(String id){
        Call<FavResponse> call = service.deleteFav(id);
        call.enqueue(new Callback<FavResponse>() {
            @Override
            public void onResponse(Call<FavResponse> call, Response<FavResponse> response) {
                if (response.isSuccessful()) {
                    Log.e("RequestError", response.message());
                    //Snackbar.make(view,"Añadido a favorito", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                } else {
                    Log.e("Delete", response.message());
                }
            }
            @Override
            public void onFailure(Call<FavResponse> call, Throwable t) {
                Log.e("RequestError", "onFailure");
            }
        });
    }
}
