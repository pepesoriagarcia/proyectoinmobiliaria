package com.pepe.housestore.dialogos;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.pepe.housestore.Listener.DialogListener;
import com.pepe.housestore.R;
import com.pepe.housestore.model.CategoryResponse;
import com.pepe.housestore.model.Propiedad;
import com.pepe.housestore.model.ResponseContainer;
import com.pepe.housestore.model.propiedad.MyPropertiesResponse;
import com.pepe.housestore.retrofit.generator.ServiceGenerator;
import com.pepe.housestore.retrofit.service.CategoryService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddMyProperti extends DialogFragment implements DialogListener {
    private static final String TAG = "addDialog";

    private EditText titulo, precio, hab, tamaño, direccion, codigoP, ciudad, provincia, descripcion,loc;
    private String idCategoria;
    private  View view;
    private Context ctx;
    private DialogListener mListener;

    private Spinner spinnerCategoria;
    private List<CategoryResponse> listacategoriaResponse = null;
    private List<String> listaCategorias = null;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        view = getActivity().getLayoutInflater().inflate(R.layout.dialog_create_properti,null);

        builder.setTitle("Añadir Propiedad")
                //.setMessage("Estas apunto de borrar la propiedad "+propertiName)
                .setPositiveButton("Añadir", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String titulo_txt = titulo.getText().toString();
                        String precio_txt = precio.getText().toString();
                        String hab_txt = hab.getText().toString();
                        String tamaño_txt = tamaño.getText().toString();
                        String direccion_txt = direccion.getText().toString();
                        String codigo_txt = codigoP.getText().toString();
                        String ciudad_txt = ciudad.getText().toString();
                        String provincia_txt = provincia.getText().toString();
                        String descripcion_txt = descripcion.getText().toString();
                        String localizacion_txt = loc.getText().toString();
                        if(!titulo_txt.isEmpty() && !precio_txt.isEmpty() && !hab_txt.isEmpty() && !tamaño_txt.isEmpty() &&
                                !idCategoria.isEmpty() && !direccion_txt.isEmpty() && !codigo_txt.isEmpty() && !ciudad_txt.isEmpty() &&
                                !provincia_txt.isEmpty() && !descripcion_txt.isEmpty() && !localizacion_txt.isEmpty()){

                            Log.e(TAG, "Valores okey" );
                            Propiedad p = new Propiedad(
                                    titulo_txt, descripcion_txt, Integer.parseInt(precio_txt), Integer.parseInt(hab_txt),
                                    Integer.parseInt(tamaño_txt), idCategoria, direccion_txt, codigo_txt, ciudad_txt,
                                    provincia_txt, localizacion_txt);


                            mListener = (DialogListener) ctx;
                            mListener.addProperti(p);
                        }else{
                            Log.e(TAG, "Valores nulos" );
                            Snackbar.make(view, "Complete los campos", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        }
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        titulo.setText("");
                        precio.setText("");
                        hab.setText("");
                        tamaño.setText("");
                        spinnerCategoria.setSelection(0);
                        direccion.setText("");
                        codigoP.setText("");
                        ciudad.setText("");
                        provincia.setText("");
                        descripcion.setText("");
                        loc.setText("");
                    }
                });

        loc = view.findViewById(R.id.loc);
        titulo = view.findViewById(R.id.title);
        precio = view.findViewById(R.id.priced);
        hab = view.findViewById(R.id.rooms);
        tamaño = view.findViewById(R.id.size_dialogo);
        direccion = view.findViewById(R.id.addressd);
        codigoP = view.findViewById(R.id.zipcoded);
        ciudad = view.findViewById(R.id.cityd);
        provincia = view.findViewById(R.id.provincedd);
        descripcion = view.findViewById(R.id.descripciond);

        spinnerCategoria = view.findViewById(R.id.categoria_add);
        listacategoriaResponse = new ArrayList<>();
        listaCategorias = new ArrayList<>();

        this.getAllCategory();

        spinnerCategoria.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                for(int c=0;c<listacategoriaResponse.size();c++){
                    if(parent.getItemAtPosition(position).toString().equals(listacategoriaResponse.get(c).getName())){
                        idCategoria = listacategoriaResponse.get(c).getId();
                        Log.e("Id: ",idCategoria );
                    }
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Log.e("ok", "ey");
            }
        });

        builder.setView(view);
        return builder.create();
    }

    @Override
    public void deleteProperti() {

    }

    @Override
    public void addProperti(Propiedad p) {

    }

    @Override
    public void editProperti(Propiedad p, String id) {

    }

    public void contexto(Context ctx){
        this.ctx = ctx;
    }

    public void getAllCategory(){
        CategoryService service = ServiceGenerator.createService(CategoryService.class);
        Call<ResponseContainer<CategoryResponse>> call = service.getCategory();
        call.enqueue(new Callback<ResponseContainer<CategoryResponse>>() {
            @Override
            public void onResponse(Call<ResponseContainer<CategoryResponse>> call, Response<ResponseContainer<CategoryResponse>> response) {
                if (response.isSuccessful()) {
                    listacategoriaResponse = response.body().getRows();

                    listaCategorias.add("**Categorias**");
                    for (int i=0;i<listacategoriaResponse.size();i++){
                        listaCategorias.add(listacategoriaResponse.get(i).getName());
                    }

                    ArrayAdapter<String> comboAdapter = new ArrayAdapter<>(ctx,android.R.layout.simple_spinner_item, listaCategorias);
                    comboAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                    spinnerCategoria.setAdapter(comboAdapter);
                    spinnerCategoria.setSelection(0);

                    Log.e("Todo bien", response.message());
                } else {
                    Log.e("RequestError", response.message());
                }
            }
            @Override
            public void onFailure(Call<ResponseContainer<CategoryResponse>> call, Throwable t) {
                Log.e("RequestError", "onFailure");
            }
        });
    }

}
