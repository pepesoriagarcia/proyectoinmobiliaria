package com.pepe.housestore.retrofit.service;

import com.pepe.housestore.model.ResponseContainer;
import com.pepe.housestore.model.meResponse;
import com.pepe.housestore.model.propiedad.MyPropertiesResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface UserService {
    @GET("/users/me")
    Call<meResponse> me();
}
