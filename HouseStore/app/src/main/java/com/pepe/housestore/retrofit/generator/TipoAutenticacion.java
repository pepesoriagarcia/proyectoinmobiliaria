package com.pepe.housestore.retrofit.generator;

public enum TipoAutenticacion {
    SIN_AUTENTICACION, BASIC, JWT
}
