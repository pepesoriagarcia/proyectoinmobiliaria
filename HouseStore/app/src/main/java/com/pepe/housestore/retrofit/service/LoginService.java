package com.pepe.housestore.retrofit.service;

import com.pepe.housestore.model.LoginResponse;
import com.pepe.housestore.model.Registro;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface LoginService {
    @POST("/auth")
    Call<LoginResponse> doLogin(@Header("Authorization") String authorization);

    @POST("/users")
    Call<LoginResponse> doRegister(@Body Registro registro);
}
