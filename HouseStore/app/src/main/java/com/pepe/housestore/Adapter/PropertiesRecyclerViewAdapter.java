package com.pepe.housestore.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.pepe.housestore.DetailPropertyActivity;
import com.pepe.housestore.Fragment.Mapas;
import com.pepe.housestore.R;
import com.pepe.housestore.Listener.PropertiesInteractionListener;
import com.pepe.housestore.retrofitService.RetrofitFav;
import com.pepe.housestore.model.propiedad.PropertiesResponse;
import com.pepe.housestore.retrofit.Utils;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link PropertiesResponse} and makes a call to the
 * specified {@link PropertiesInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class PropertiesRecyclerViewAdapter extends RecyclerView.Adapter<PropertiesRecyclerViewAdapter.ViewHolder> {

    private final List<PropertiesResponse> mValues;
    private final PropertiesInteractionListener mListener;
    private final Context ctx;
    private int fragmentFavorite;


    public PropertiesRecyclerViewAdapter(Context ctx, List<PropertiesResponse> items, PropertiesInteractionListener listener, int fragmentFavorite) {
        mValues = items;
        mListener = listener;
        this.ctx = ctx;
        this.fragmentFavorite = fragmentFavorite;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_properties, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mValues.get(position);
        //holder.mIdView.setText(mValues.get(position).id);
        //holder.mContentView.setText(mValues.get(position).content);
        holder.titulo.setText(mValues.get(position).getTitle());
        holder.price.setText(Integer.toString(mValues.get(position).getSize())+" €/mes");
        holder.room.setText(Integer.toString(mValues.get(position).getRooms())+" hab.");
        holder.size.setText(Integer.toString(mValues.get(position).getSize())+" m2");

        //foto miniatura-----------------
        if( mValues.get(position).getOnePhotos() != null){
            //holder.miniatura.setScaleType(ImageView.ScaleType.CENTER_CROP);
            Glide
                    .with(ctx)
                    .load(mValues.get(position).getOnePhotos())
                    //.apply(new RequestOptions().override(100, 100).placeholder(R.drawable.placeHolder).error(R.drawable.error_pic))
                    //.apply(new RequestOptions().placeholder(R.drawable.ic_delete_black_24dp).override(100,100)
                    .into(holder.miniatura);
        }
        //foto user------------------
        if(mValues.get(position).getAvatar() != null){
            //Log.e("avatar",mValues.get(position).getOwnerId().getPicture());
            //holder.avatarUser.setScaleType(ImageView.ScaleType.CENTER_CROP);
           /* Glide
                    .with(ctx)
                    .load(mValues.get(position).getAvatar())
                    .into(holder.avatarUser);*/
        }
        //corazon------------------------
        /*Log.e("id: ", String.valueOf(mValues.get(position).getId()));
        Log.e("fav: ", String.valueOf(mValues.get(position).getisFav()));*/
        if(!Utils.getToken(ctx).isEmpty() && mValues.get(position).getisFav() == true){
            holder.faborit.setScaleType(ImageView.ScaleType.CENTER_CROP);
            Glide.with(ctx).load(R.drawable.ic_favorite_red_24dp).into(holder.faborit);
        }
        //---------------------------------
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {

                    Bundle parmetros = new Bundle();
                    parmetros.putString("id", mValues.get(position).getId());
                    parmetros.putBoolean("favorite", mValues.get(position).getisFav());
                    parmetros.putInt("ListFavorite",fragmentFavorite);

                    Intent i = new Intent(ctx, DetailPropertyActivity.class);
                    i.putExtras(parmetros);
                    ctx.startActivity(i);
                }
            }
        });
        holder.faborit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    if(mValues.get(position).getisFav() == false){
                        RetrofitFav r = new RetrofitFav(ctx);
                        r.addFav(mValues.get(position).getId());
                        mValues.get(position).setisFav(true);
                        Glide
                                .with(ctx)
                                .load(R.drawable.ic_favorite_red_24dp)
                                //.load(R.drawable.lista)
                                .into(holder.faborit);
                    }
                    else {
                        RetrofitFav r = new RetrofitFav(ctx);
                        r.deleteFav(mValues.get(position).getId());

                        Glide
                                .with(ctx)
                                .load(R.drawable.ic_favorite_border_black_24dp)
                                //.load(R.drawable.lista)
                                .into(holder.faborit);
                    }
                }
            }
        });
        holder.mapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.abrirMapa(mValues.get(position));
                    //mValues.get(position)
                }
            }
        });
        if (mValues.get(position).getCategoryId() != null) {
            if(mValues.get(position).getCategoryId().getName().equals("Alquiler")){
                holder.price.setText(String.valueOf(mValues.get(position).getPrice())+" €/mes");
            }else if(mValues.get(position).getCategoryId().getName().equals("Obra nueva") || mValues.get(position).getCategoryId().getName().equals("Venta")){
                holder.price.setText(String.valueOf(mValues.get(position).getPrice())+" €");
            }else{
                holder.price.setText(String.valueOf(mValues.get(position).getPrice())+" €");
            }
            holder.category.setText(mValues.get(position).getCategoryId().getName());
        }
        else{
            holder.price.setText(String.valueOf(mValues.get(position).getPrice())+" €");
        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        //public final TextView mIdView;
        //public final TextView mContentView;
        public TextView titulo, price, room, size, descripcion, category;
        public ImageView miniatura, mapa, faborit, avatarUser;

        public PropertiesResponse mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            //mIdView = (TextView) view.findViewById(R.id.item_number);
            //mContentView = (TextView) view.findViewById(R.id.content);
            titulo = view.findViewById(R.id.titulo);
            price = view.findViewById(R.id.priced);
            room = view.findViewById(R.id.room);
            size = view.findViewById(R.id.size);
            category = view.findViewById(R.id.category);

            //descripcion = view.findViewById(R.id.descripcion);

            miniatura = view.findViewById(R.id.miniatura);
            mapa = view.findViewById(R.id.mapa);
            faborit = view.findViewById(R.id.faborit);
            avatarUser = view.findViewById(R.id.avatarUser);

            if(Utils.getToken(ctx).isEmpty() || fragmentFavorite == 1){
                faborit.setVisibility(view.GONE);
            }
        }

        @Override
        public String toString() {
            return "ViewHolder{" +
                    "mView=" + mView +
                    ", titulo=" + titulo +
                    ", price=" + price +
                    ", room=" + room +
                    ", size=" + size +
                    ", descripcion=" + descripcion +
                    ", miniatura=" + miniatura +
                    ", mapa=" + mapa +
                    ", faborit=" + faborit +
                    ", deleteOuser=" + avatarUser +
                    ", mItem=" + mItem +
                    '}';
        }
    }
}
