package com.pepe.housestore.model.propiedad;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OwnerId{
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("napictureme")
    @Expose
    private String picture;
    @SerializedName("name")
    @Expose
    private String name;

    public OwnerId(){}
    public OwnerId(String id, String picture, String name) {
        this.id = id;
        this.picture = picture;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
