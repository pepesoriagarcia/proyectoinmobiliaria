package com.pepe.housestore;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.pepe.housestore.Fragment.Mapas;
import com.pepe.housestore.Fragment.MyPropertiesFragment;
import com.pepe.housestore.Fragment.PropertiesFavoriteFragment;
import com.pepe.housestore.Fragment.PropertiesFragment;
import com.pepe.housestore.Listener.DialogListener;
import com.pepe.housestore.Listener.MapasInteractionListener;
import com.pepe.housestore.Listener.PropertiesInteractionListener;
import com.pepe.housestore.Listener.Recargar;
import com.pepe.housestore.dialogos.DeleteMyProperti;
import com.pepe.housestore.dialogos.Filtro;
import com.pepe.housestore.dialogos.AddMyProperti;
import com.pepe.housestore.dialogos.editMyProperti;
import com.pepe.housestore.model.Propiedad;
import com.pepe.housestore.model.propiedad.MyPropertiesResponse;
import com.pepe.housestore.model.propiedad.PropertiesResponse;
import com.pepe.housestore.retrofit.Utils;
import com.pepe.housestore.retrofitService.RetrofitMyProperties;

public class MainActivity extends AppCompatActivity implements PropertiesInteractionListener, MapasInteractionListener, DialogListener, Recargar {
    private FloatingActionButton fab;
    private String id;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            BottomNavigationView n = (BottomNavigationView) findViewById(R.id.navigation);

            Menu menu = n.getMenu();
            menu.findItem(R.id.navigation_home).setIcon(R.drawable.ic_outline_home_24px);
            menu.findItem(R.id.navigation_favorite).setIcon(R.drawable.ic_favorite_border_blanco_24dp);
            menu.findItem(R.id.navigation_my_properties).setIcon(R.drawable.ic_outline_featured_play_list_24px);
            menu.findItem(R.id.navigation_map).setIcon(R.drawable.ic_maps_blan);

            fab = (FloatingActionButton) findViewById(R.id.addProperti);

            switch (item.getItemId()) {
                case R.id.navigation_home:
                    item.setIcon(R.drawable.ic_home_black_24dp);
                    fab.hide();
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.containerFragment, new PropertiesFragment(), "listProperties")
                            .commit();
                    return true;
                case R.id.navigation_favorite:
                    item.setIcon(R.drawable.ic_favorite_black_24dp);
                    fab.hide();
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.containerFragment, new PropertiesFavoriteFragment(), "listFavoriteProperties")
                            .commit();
                    return true;
                case R.id.navigation_my_properties:
                    item.setIcon(R.drawable.ic_featured_play_list_black_24dp);
                    fab.show();
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.containerFragment, new MyPropertiesFragment(), "listMyProperties")
                            .commit();
                    return true;
                case R.id.navigation_map:
                    item.setIcon(R.drawable.ic_maps_negro);
                    fab.hide();
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.containerFragment, new Mapas(), "map")
                            .commit();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fab = (FloatingActionButton) findViewById(R.id.addProperti);
        fab.hide();

        //mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        if(Utils.getToken(this).isEmpty()){
            navigation.setVisibility(View.GONE);
        }

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.containerFragment, new PropertiesFragment(), "listProperties")
                .commit();
        Menu menu = navigation.getMenu();
        menu.findItem(R.id.navigation_home).setIcon(R.drawable.ic_home_black_24dp);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(!Utils.getToken(MainActivity.this).isEmpty()){
            getMenuInflater().inflate(R.menu.menu_superior, menu);
        }
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.filtar:
                //abrir dialogo de filtro
                Filtro filtro = new Filtro();
                filtro.setCtx(MainActivity.this);
                filtro.show(getSupportFragmentManager(), "dialog delete");
                return true;
            case R.id.user:
                startActivity(new Intent(MainActivity.this, UserActivity.class));
                return true;
            case R.id.logout:
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                //borrar el token ya lo hace el onCreate de login
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public void añadirPropertiOpenDialog(View view){
        AddMyProperti addProperti = new AddMyProperti();
        addProperti.contexto(MainActivity.this);
        addProperti.show(MainActivity.this.getSupportFragmentManager (), "addDialog");
    }


    @Override
    public void abrirMapa(PropertiesResponse p) {
        Mapas fMapas = new Mapas();

        String localizacion = p.getLoc();
        String[] parts = localizacion.split(",");
        String lat = parts[0];
        String lon = parts[1];

        Bundle m = new Bundle();
        m.putDouble("lat", Double.parseDouble(lat));
        m.putDouble("lon", Double.parseDouble(lon));
        m.putString("direccion",p.getAddress());
        m.putInt("desde",1);
        fMapas.setArguments(m);

        getSupportFragmentManager().beginTransaction().replace(R.id.containerFragment, fMapas, "map").commit();
    }

    @Override
    public void edit(MyPropertiesResponse p) {
        Log.e("id_cat_main: ", String.valueOf(p.getCategoryid()));
        if(p != null){
            DialogFragment e = new editMyProperti();
            ((editMyProperti) e).contexto(MainActivity.this);
            ((editMyProperti) e).setId(p.getId());
            Propiedad propiedad = new Propiedad(
                  p.getTitle(),p.getDescription(),p.getPrice(),
                  p.getRooms(),p.getSize(),"0",
                  p.getAddress(),p.getZipcode(),p.getCity(),
                    p.getProvince(),p.getLoc()
            );
            ((editMyProperti) e).setP(propiedad);
            e.show(MainActivity.this.getSupportFragmentManager(),"edit");
        }
    }

    @Override
    public void delete(String id) {
        this.id = id;
        DialogFragment d = new DeleteMyProperti();
        ((DeleteMyProperti) d).contexto(MainActivity.this);
        d.show(MainActivity.this.getSupportFragmentManager (), "delete");
    }

    @Override
    public void deleteProperti() {
        RetrofitMyProperties r = new RetrofitMyProperties(MainActivity.this);
        r.deleteProperti(id);
    }

    @Override
    public void addProperti(Propiedad p) {
        RetrofitMyProperties r = new RetrofitMyProperties(MainActivity.this);
        r.addProperti(p);
    }

    @Override
    public void editProperti(Propiedad p, String id) {
        RetrofitMyProperties r = new RetrofitMyProperties(MainActivity.this);
        r.editProperti(id, p);
    }

    @Override
    public void recargar() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.containerFragment, new MyPropertiesFragment(), "listMyProperties")
                .commit();
    }
}
