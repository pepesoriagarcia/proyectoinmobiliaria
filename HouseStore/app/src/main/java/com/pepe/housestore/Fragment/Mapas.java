package com.pepe.housestore.Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.pepe.housestore.Adapter.PropertiesRecyclerViewAdapter;
import com.pepe.housestore.Listener.MapasInteractionListener;
import com.pepe.housestore.R;
import com.pepe.housestore.model.Propiedad;
import com.pepe.housestore.model.ResponseContainer;
import com.pepe.housestore.model.propiedad.PropertiesResponse;
import com.pepe.housestore.retrofit.Utils;
import com.pepe.housestore.retrofit.generator.ServiceGenerator;
import com.pepe.housestore.retrofit.generator.TipoAutenticacion;
import com.pepe.housestore.retrofit.service.PropertiesService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.lang.Thread.sleep;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MapasInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Mapas#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Mapas extends Fragment implements OnMapReadyCallback {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private GoogleMap map;
    private MapView mapView;
    private View view;
    private List<PropertiesResponse> listaPropiedades;

    private Double pLat=37.37915859822854, pLon=-5.994812262477609;
    private String pDireccion=null;
    private int desde;

    private MapasInteractionListener mListener;

    public Mapas() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Mapas.
     */
    // TODO: Rename and change types and number of parameters
    public static Mapas newInstance(String param1, String param2) {
        Mapas fragment = new Mapas();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            pLat = getArguments().getDouble("lat",0);
            pLon = getArguments().getDouble("lon",0);
            pDireccion = getArguments().getString("direccion", null);
            desde = getArguments().getInt("desde");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_mapas, container, false);


        return view;
    }



    @Override
    public void onViewCreated( View view,  Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapView = view.findViewById(R.id.map);
        if(mapView != null){
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    /*public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.MapasInteractionListener(uri);
        }
    }*/

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof MapasInteractionListener) {
            mListener = (MapasInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement MapasInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        //this.getProperties();
        MapsInitializer.initialize(getContext());

        map = googleMap;

        LatLng ubi = new LatLng(pLat, pLon);
        map.addMarker(new MarkerOptions()
                        .position(ubi)
                        .title(pDireccion)
                //.snippet("Esta es la ubicacion")
        );
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(ubi,5));
        /*if(desde == 1){
            Log.e("desde: ",String.valueOf(desde));
            for(int d=0;d<listaPropiedades.size();d++){
                String localizacion = listaPropiedades.get(d).getLoc();
                String[] parts = localizacion.split(",");
                String lat_t = parts[0];
                String lon_t = parts[1];

                Log.e("lista: ",listaPropiedades.get(d).getTitle());

                map = googleMap;

                LatLng ubi = new LatLng(Double.parseDouble(lat_t), Double.parseDouble(lon_t));
                map.addMarker(new MarkerOptions().position(ubi).title(listaPropiedades.get(d).getTitle()));

                map.moveCamera(CameraUpdateFactory.newLatLngZoom(ubi,12));
            }
        }else {
            map = googleMap;

            Log.e("desde: ", String.valueOf(desde));
            LatLng ubi = new LatLng(pLat, pLon);
            map.addMarker(new MarkerOptions()
                            .position(ubi)
                            .title(pDireccion)
                    //.snippet("Esta es la ubicacion")
            );
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(ubi,5));
        }*/
    }
    /*public void cargarUbicacion(GoogleMap googleMap,Double lat,Double lon,String calle){
        MapsInitializer.initialize(getContext());
        map = googleMap;

        map = googleMap;
        // Add a marker in Sydney and move the camera
        LatLng ubi = new LatLng(lat, lon);
        map.addMarker(new MarkerOptions()
                .position(ubi)
                .title(calle));
        //.snippet("Esta es la ubicacion")
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(ubi,12));
    }*/
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    /*public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }*/
    public void getProperties(){
        PropertiesService service = ServiceGenerator.createService(PropertiesService.class);
        Call<ResponseContainer<PropertiesResponse>> call = service.listaPropertiesM();
        call.enqueue(new Callback<ResponseContainer<PropertiesResponse>>() {
            @Override
            public void onResponse(Call<ResponseContainer<PropertiesResponse>> call, Response<ResponseContainer<PropertiesResponse>> response) {
                if (response.isSuccessful()) { listaPropiedades = response.body().getRows(); }
                else { Log.e("RequestError", response.message()); }
            }
            @Override
            public void onFailure(Call<ResponseContainer<PropertiesResponse>> call, Throwable t) {
                Log.e("RequestError", "onFailure");
            }
        });
    }
}
