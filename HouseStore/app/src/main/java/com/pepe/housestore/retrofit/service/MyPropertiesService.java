package com.pepe.housestore.retrofit.service;

import com.pepe.housestore.model.Propiedad;
import com.pepe.housestore.model.ResponseContainer;
import com.pepe.housestore.model.propiedad.MyPropertiesResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface MyPropertiesService {
    @GET("/properties/mine")
    Call<ResponseContainer<MyPropertiesResponse>> myProperties();

    @POST("/properties")
    Call<Propiedad> addProperti(@Body Propiedad propiedad);

    @DELETE("/properties/{id}")
    Call<String> deleteProperti(@Path("id") String id);

    @PUT("/properties/{id}")
    Call<Propiedad> editProperti(@Path("id") String id, @Body Propiedad propiedad);
}
