package com.pepe.housestore;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.pepe.housestore.model.LoginResponse;
import com.pepe.housestore.model.Registro;
import com.pepe.housestore.retrofit.Utils;
import com.pepe.housestore.retrofit.generator.ServiceGenerator;
import com.pepe.housestore.retrofit.service.LoginService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistroActivity extends AppCompatActivity {
    EditText name, email, pass1, pass2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        name = findViewById(R.id.name_u);
        email = findViewById(R.id.email_u);
        pass1 = findViewById(R.id.pass1);
        pass2 = findViewById(R.id.pass2);

        Utils.setData(this,"","","","","");
    }
    public void directoLogin(View view){
        startActivity(new Intent(RegistroActivity.this, LoginActivity.class));
        finish();
    }
    public void registro(final View view){
        String fullname_txt = name.getText().toString().trim();
        String email_txt = email.getText().toString().trim();
        String password1_txt = pass1.getText().toString().trim();
        String password2_txt = pass2.getText().toString().trim();

        if(!fullname_txt.isEmpty() && !email_txt.isEmpty() && !password1_txt.isEmpty() && !password2_txt.isEmpty()){
            if(password1_txt.equals(password2_txt)){
                Registro registro = new Registro(fullname_txt, email_txt, password1_txt);

                LoginService service = ServiceGenerator.createService(LoginService.class);

                Call<LoginResponse> loginReponseCall = service.doRegister(registro);

                loginReponseCall.enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                        Utils.setData(
                                RegistroActivity.this,
                                response.body().getToken(),
                                response.body().getUser().getId(),
                                response.body().getUser().getEmail(),
                                response.body().getUser().getName(),
                                response.body().getUser().getPicture()
                        );
                        startActivity(new Intent(RegistroActivity.this, MainActivity.class));
                        finish();
                    }
                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        Snackbar.make(view,"Error en peticion", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        pass1.setText("");
                        pass2.setText("");
                    }
                });
            }
            else {
                Snackbar.make(view,"Confirme Contraseña", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                Log.e("pass 1", password1_txt);
                Log.e("pass 2", password2_txt);
                Log.e("email", fullname_txt);
            }
        }
        else{
            Snackbar.make(view,"Completa todos los campos", Snackbar.LENGTH_LONG).setAction("Action", null).show();
        }
    }
}
