package com.pepe.housestore.retrofit.service;

import com.pepe.housestore.model.ResponseContainer;
import com.pepe.housestore.model.ResponseContainerNoList;
import com.pepe.housestore.model.propiedad.PropertiesResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface PropertiesService {
    @GET("/properties")
    Call<ResponseContainer<PropertiesResponse>> listaPropertiesM();

    @GET("/properties/auth")
    Call<ResponseContainer<PropertiesResponse>> listaPropertiesT();

    @GET("/properties/{id}")
    Call<ResponseContainerNoList<PropertiesResponse>> oneProperti(@Path("id") String id);

    @GET("/properties/fav")
    Call<ResponseContainer<PropertiesResponse>> listaPropertiesFav();
}
