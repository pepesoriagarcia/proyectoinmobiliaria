package com.pepe.housestore.dialogos;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;

import com.pepe.housestore.R;
import com.pepe.housestore.model.CategoryResponse;
import com.pepe.housestore.model.ResponseContainer;
import com.pepe.housestore.retrofit.generator.ServiceGenerator;
import com.pepe.housestore.retrofit.service.CategoryService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Filtro extends DialogFragment {
    private EditText ciudad,provincia,calle;
    private Spinner spinnerCategoria;
    private SeekBar dinero;


    private List<CategoryResponse> listacategoriaResponse = null;
    private List<String> listaCategorias = null;
    private Context ctx;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        View v = getActivity().getLayoutInflater().inflate(R.layout.dialog_filtro,null);

        ciudad = v.findViewById(R.id.ciudad);
        provincia = v.findViewById(R.id.provincia);
        calle = v.findViewById(R.id.calle);
        dinero = v.findViewById(R.id.dinero);

        spinnerCategoria = v.findViewById(R.id.filtro_categoria_dialog);
        listacategoriaResponse = new ArrayList<>();
        listaCategorias = new ArrayList<>();

        builder.setTitle("Filtro")
                //.setMessage("Estas apunto de borrar la propiedad "+propertiName)
                .setPositiveButton("Filtrar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String ciu = ciudad.getText().toString();
                        String pro = provincia.getText().toString();
                        String cal = calle.getText().toString();
                        String cate = spinnerCategoria.getSelectedItem().toString();
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        this.getAllCategory();


        spinnerCategoria.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.e("onItemSelected: ",parent.getItemAtPosition(position).toString() );
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Log.e("ok", "ey");
            }
        });

        dinero.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress_value;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progress_value = progress;
                Log.e("onItemSelected: ", String.valueOf(progress));
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        builder.setView(v);
        return builder.create();
    }
    public void setCtx(Context ctx) {
        this.ctx = ctx;
    }
    public void getAllCategory(){
        CategoryService service = ServiceGenerator.createService(CategoryService.class);
        Call<ResponseContainer<CategoryResponse>> call = service.getCategory();
        call.enqueue(new Callback<ResponseContainer<CategoryResponse>>() {
            @Override
            public void onResponse(Call<ResponseContainer<CategoryResponse>> call, Response<ResponseContainer<CategoryResponse>> response) {
                if (response.isSuccessful()) {
                    listacategoriaResponse = response.body().getRows();

                    listaCategorias.add("**Seleccione Categoria**");
                    for (int i=0;i<listacategoriaResponse.size();i++){
                        listaCategorias.add(listacategoriaResponse.get(i).getName());
                    }

                    ArrayAdapter<String> comboAdapter = new ArrayAdapter<>(ctx,android.R.layout.simple_spinner_item, listaCategorias);
                    comboAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                    spinnerCategoria.setAdapter(comboAdapter);
                    spinnerCategoria.setSelection(0);

                    Log.e("Todo bien", response.message());
                } else {
                    Log.e("RequestError", response.message());
                }
            }
            @Override
            public void onFailure(Call<ResponseContainer<CategoryResponse>> call, Throwable t) {
                Log.e("RequestError", "onFailure");
            }
        });
    }
}
