package com.pepe.housestore.model.propiedad;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pepe.housestore.R;

public class PropertiesResponse {
    @SerializedName("loc")
    @Expose
    private String loc;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("price")
    @Expose
    private int price;
    @SerializedName("rooms")
    @Expose
    private int rooms;
    @SerializedName("size")
    @Expose
    private int size;
    @SerializedName("categoryId")
    @Expose
    private Category categoryId;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("zipcode")
    @Expose
    private String zipcode;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("province")
    @Expose
    private String province;
    @SerializedName("ownerId")
    @Expose
    private OwnerId ownerId;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("__v")
    @Expose
    private int __v;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("photos")
    @Expose
    private String photos[];
    @SerializedName("isFav")
    @Expose
    private boolean isFav;


    public PropertiesResponse(String loc, String title, String description, int price, int rooms,
                              int size, Category categoryId, String address, String zipcode,
                              String city, String province, OwnerId ownerId, String createdAt,
                              String updatedAt, int __v, String id, String[] photos, boolean isFav) {
        this.loc = loc;
        this.title = title;
        this.description = description;
        this.price = price;
        this.rooms = rooms;
        this.size = size;
        this.categoryId = categoryId;
        this.address = address;
        this.zipcode = zipcode;
        this.city = city;
        this.province = province;
        this.ownerId = ownerId;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.__v = __v;
        this.id = id;
        this.photos = photos;
        this.isFav = isFav;
    }

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getRooms() {
        return rooms;
    }

    public void setRooms(int rooms) {
        this.rooms = rooms;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public Category getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Category categoryid) {
        this.categoryId = categoryId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public OwnerId getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(OwnerId ownerId) {
        this.ownerId = ownerId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int get__v() {
        return __v;
    }

    public void set__v(int __v) {
        this.__v = __v;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String[] getPhotos() {
        return photos;
    }

    public void setPhotos(String[] photos) {
        this.photos = photos;
    }

    public boolean getisFav() {
        return isFav;
    }

    public void setisFav(boolean idFav) {
        this.isFav = idFav;
    }

    public String getOnePhotos() {
        String p = null;
        if (photos != null){
            p = photos[0];
        }
        return p;
    }
    public String getAvatar(){
        String r = null;
        if(ownerId.getPicture() != null){
            r = ownerId.getPicture();
        }
        return r;
    }
}
