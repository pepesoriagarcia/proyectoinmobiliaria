package com.pepe.housestore.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pepe.housestore.DetailPropertyActivity;

import com.pepe.housestore.Listener.PropertiesInteractionListener;
import com.pepe.housestore.R;
import com.pepe.housestore.model.propiedad.MyPropertiesResponse;
import com.pepe.housestore.model.propiedad.PropertiesResponse;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link MyPropertiesResponse} and makes a call to the
 * specified {@link PropertiesInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyPropertiesRecyclerViewAdapter extends RecyclerView.Adapter<MyPropertiesRecyclerViewAdapter.ViewHolder> implements PropertiesInteractionListener{

    private final List<MyPropertiesResponse> mValues;
    private final PropertiesInteractionListener mListener;
    private final Context ctx;

    public MyPropertiesRecyclerViewAdapter(Context ctx,List<MyPropertiesResponse> items, PropertiesInteractionListener listener,int layout) {
        mValues = items;
        mListener = listener;
        this.ctx = ctx;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_myproperties, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder,final int position) {
        holder.mItem = mValues.get(position);
        /*holder.mIdView.setText(mValues.get(position).id);
        holder.mContentView.setText(mValues.get(position).content);*/
        holder.titulo.setText(mValues.get(position).getTitle());
        holder.price.setText(Integer.toString(mValues.get(position).getSize())+" €/mes");
        holder.room.setText(Integer.toString(mValues.get(position).getRooms())+" hab.");
        holder.size.setText(Integer.toString(mValues.get(position).getSize())+" m2");
        holder.descripcion.setText(mValues.get(position).getDescription());


        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if (null != mListener) {
                    String id = mValues.get(position).getId();

                    Bundle parmetros = new Bundle();
                    parmetros.putString("id", id);
                    parmetros.putBoolean("favorite", false);
                    parmetros.putBoolean("esMia",true);

                    Intent i = new Intent(ctx, DetailPropertyActivity.class);
                    i.putExtras(parmetros);
                    ctx.startActivity(i);
                }
            }
        });
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    //DialogFragment deleteMyProperti = new DeleteMyProperti();
                    //deleteMyProperti.show(ctx.getSupportFragmentManager(), "delete");
                    mListener.delete(mValues.get(position).getId());
                }
            }
        });
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    //DialogFragment deleteMyProperti = new DeleteMyProperti();
                    //deleteMyProperti.show(ctx.getSupportFragmentManager(), "delete");
                    //Log.e("Este es el puto click", String.valueOf(mValues.get(position).getCategoryid()));
                    mListener.edit(mValues.get(position));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    @Override
    public void delete(String id) {

    }

    @Override
    public void abrirMapa(PropertiesResponse p) {

    }

    @Override
    public void edit(MyPropertiesResponse p) {

    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        /*public final TextView mIdView;
        public final TextView mContentView;*/
        public MyPropertiesResponse mItem;
        public TextView titulo, price, room, size, descripcion;
        public ImageView edit, delete;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            /*mIdView = (TextView) view.findViewById(R.id.item_number);
            mContentView = (TextView) view.findViewById(R.id.content);*/
            edit = view.findViewById(R.id.edit);
            delete = view.findViewById(R.id.delete);

            titulo = view.findViewById(R.id.titulo);
            price = view.findViewById(R.id.priced);
            room = view.findViewById(R.id.room);
            size = view.findViewById(R.id.size);
            descripcion = view.findViewById(R.id.description);
        }
    }
}
