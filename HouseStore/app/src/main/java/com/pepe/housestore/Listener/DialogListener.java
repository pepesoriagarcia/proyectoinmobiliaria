package com.pepe.housestore.Listener;

import com.pepe.housestore.model.Propiedad;
import com.pepe.housestore.model.propiedad.MyPropertiesResponse;

import java.util.Properties;

public interface DialogListener {
    void deleteProperti();
    void addProperti(Propiedad p);
    void editProperti(Propiedad p, String id);
}
