package com.pepe.housestore;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.pepe.housestore.model.LoginResponse;
import com.pepe.housestore.retrofit.Utils;
import com.pepe.housestore.retrofit.generator.ServiceGenerator;
import com.pepe.housestore.retrofit.service.LoginService;

import okhttp3.Credentials;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    Button btnLogin;
    EditText email, pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btnLogin = findViewById(R.id.btn_login);
        email = findViewById(R.id.email_u);
        pass = findViewById(R.id.password);

        Utils.setData(this,"","","","","");


        email.setText("p@p.com");
        pass.setText("123456");
    }
    public void directoRegistro(View view){
        startActivity(new Intent(LoginActivity.this, RegistroActivity.class));
        finish();
    }
    public void sinLogin(View view){
        startActivity(new Intent(LoginActivity.this, MainActivity.class));
        finish();
    }
    public void Login(final View view){
        String username_txt = email.getText().toString();
        String password_txt = pass.getText().toString();


        //no funciona siempre da error en peticion
        if(!username_txt.isEmpty() && !password_txt.isEmpty()){
            String credentials = Credentials.basic(username_txt, password_txt);

            LoginService service = ServiceGenerator.createService(LoginService.class);
            Call<LoginResponse> call = service.doLogin(credentials);

            call.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    if (response.code() != 201) {
                        // error
                        Log.e("RequestError", response.message());
                        //Toast.makeText(LoginActivity.this, "Error de petición !201", Toast.LENGTH_SHORT).show();
                        Snackbar.make(view,"Error de petición !201", Snackbar.LENGTH_LONG).setAction("Action", null).show();

                        email.setText("");
                        pass.setText("");
                    }
                    else {
                        //guardar token en utils
                        Utils.setData(
                                LoginActivity.this,
                                response.body().getToken(),
                                response.body().getUser().getId(),
                                response.body().getUser().getEmail(),
                                response.body().getUser().getName(),
                                response.body().getUser().getPicture()
                        );
                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        finish();
                    }
                }
                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    Log.e("NetworkFailure", t.getMessage());
                    //Toast.makeText(LoginActivity.this, "onFailure", Toast.LENGTH_SHORT).show();
                    Snackbar.make(view,"Error de red", Snackbar.LENGTH_LONG).setAction("Action", null).show();

                    email.setText("");
                    pass.setText("");
                }
            });
        }
        else{
            Snackbar.make(view, "Complete los campos", Snackbar.LENGTH_LONG).setAction("Action", null).show();
        }
    }
}
