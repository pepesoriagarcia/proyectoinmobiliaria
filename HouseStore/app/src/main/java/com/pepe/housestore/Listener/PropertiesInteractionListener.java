package com.pepe.housestore.Listener;


import com.pepe.housestore.model.propiedad.MyPropertiesResponse;
import com.pepe.housestore.model.propiedad.PropertiesResponse;

public interface PropertiesInteractionListener {
    void delete(String id);
    void abrirMapa(PropertiesResponse p);
    void edit(MyPropertiesResponse p);
}
