package com.pepe.housestore;

import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.pepe.housestore.Fragment.Mapas;
import com.pepe.housestore.Fragment.PropertiesFragment;
import com.pepe.housestore.Listener.MapasInteractionListener;
import com.pepe.housestore.retrofitService.RetrofitFav;
import com.pepe.housestore.model.ResponseContainerNoList;
import com.pepe.housestore.model.propiedad.PropertiesResponse;
import com.pepe.housestore.retrofit.Utils;
import com.pepe.housestore.retrofit.generator.ServiceGenerator;
import com.pepe.housestore.retrofit.service.PropertiesService;
import com.smarteist.autoimageslider.DefaultSliderView;
import com.smarteist.autoimageslider.SliderLayout;
import com.smarteist.autoimageslider.SliderView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailPropertyActivity extends AppCompatActivity implements MapasInteractionListener {
    private String id;
    private boolean favorite;
    private int desdeListFavorite;
    private boolean esMia;
    private PropertiesResponse properti;

    private CollapsingToolbarLayout superior;
    private SliderLayout sliderLayout;

    private TextView price, rooms, size, city, province,zipcode, address, description, category, direccion;
    private ImageView cama, tamaño, ciudad;
    private ConstraintLayout base;

    private String lat,lon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_property);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        sliderLayout = findViewById(R.id.imageSlider);

        base = findViewById(R.id.base_detalle);

        cama = findViewById(R.id.cama);
        tamaño = findViewById(R.id.tamaño);
        ciudad = findViewById(R.id.ciurdad);
        direccion = findViewById(R.id.direccion);

        price = findViewById(R.id.priced);
        rooms = findViewById(R.id.rooms);
        size = findViewById(R.id.size);
        province = findViewById(R.id.provincedd);
        city = findViewById(R.id.cityd);
        zipcode = findViewById(R.id.zipcoded);
        address = findViewById(R.id.addressd);
        description = findViewById(R.id.description);
        category = findViewById(R.id.category);



        id = getIntent().getExtras().getString("id");
        favorite = getIntent().getExtras().getBoolean("favorite");
        desdeListFavorite = getIntent().getExtras().getInt("ListFavorite");
        esMia = getIntent().getExtras().getBoolean("esMia");

        this.ocultar();
        this.getOneProperti();

        superior = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

       /* Log.e("token", Utils.getToken(DetailPropertyActivity.this));
        Log.e("favorite", String.valueOf(favorite));
        Log.e("desdeLista", String.valueOf(desdeListFavorite));*/

        if(!Utils.getToken(DetailPropertyActivity.this).isEmpty()){
            if(favorite == true){
                fab.setImageResource(R.drawable.ic_favorite_blanco_24dp);
            }
        }else{
            fab.setImageResource(R.drawable.ic_favorite_border_blanco_24dp);
        }

        //aqui
        if(esMia == true){
            fab.setImageResource(R.drawable.ic_home_blanca_24dp);
        } else {
            if(desdeListFavorite == 1){
                favorite = true;
                fab.setImageResource(R.drawable.ic_favorite_blanco_24dp);
                fab.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        RetrofitFav r = new RetrofitFav(DetailPropertyActivity.this);
                        if(!Utils.getToken(DetailPropertyActivity.this).isEmpty() && favorite == true) {
                            fab.setImageResource(R.drawable.ic_favorite_border_blanco_24dp);
                            r.deleteFav( id);
                            favorite = false;
                        }else{
                            fab.setImageResource(R.drawable.ic_favorite_blanco_24dp);
                            r.addFav(id);
                            favorite = true;
                        }
                    }
                });
            }else{
                fab.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        RetrofitFav r = new RetrofitFav(DetailPropertyActivity.this);
                        if(!Utils.getToken(DetailPropertyActivity.this).isEmpty() && favorite == true){
                            //lanza delete fav
                            r.deleteFav(id);
                            //cambiar icon
                            fab.setImageResource(R.drawable.ic_favorite_border_blanco_24dp);
                            //no me preocupo de la variable favorite ya que cuando entre otra vez volvera a ahcer la peticion
                            favorite = false;
                        }else if(!Utils.getToken(DetailPropertyActivity.this).isEmpty() && favorite == false){
                            r.addFav(id);
                            fab.setImageResource(R.drawable.ic_favorite_blanco_24dp);
                            favorite = true;
                        }else{
                            Snackbar.make(view,"Registrate para tener esta funcion", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        }
                    }
                });
            }
        }

        //mapa
       /* Mapas fMapas = new Mapas();

        if(!properti.getLoc().isEmpty()){
            String localizacion = properti.getLoc();
            String[] parts = localizacion.split(",");
            lat = parts[0];
            lon = parts[1];
        }
        lat = "0";
        lon = "0";

        Bundle mBundle = new Bundle();
        mBundle.putDouble("lat", Double.parseDouble(lat));
        mBundle.putDouble("lon", Double.parseDouble(lon));
       mBundle.putString("direccion",properti.getAddress());

        fMapas.setArguments(mBundle);*/

    }
    public void getOneProperti(){
        PropertiesService service;
        service = ServiceGenerator.createService(PropertiesService.class);
        Call<ResponseContainerNoList<PropertiesResponse>> call = service.oneProperti(id);

        call.enqueue(new Callback<ResponseContainerNoList<PropertiesResponse>>() {
            @Override
            public void onResponse(Call<ResponseContainerNoList<PropertiesResponse>> call, Response<ResponseContainerNoList<PropertiesResponse>> response) {
                if (response.isSuccessful()) {
                    Log.e("RequestSuccessful", response.message());
                    properti = response.body().getRows();

                    //mapa
                    Mapas fMapas = new Mapas();

                    String localizacion = properti.getLoc();
                    String[] parts = localizacion.split(",");
                    lat = parts[0];
                    lon = parts[1];

                    Bundle m = new Bundle();
                    m.putDouble("lat", Double.parseDouble(lat));
                    m.putDouble("lon", Double.parseDouble(lon));
                    m.putString("direccion",properti.getAddress());
                    m.putInt("desde",0);
                    fMapas.setArguments(m);

                    getSupportFragmentManager().beginTransaction().add(R.id.MapaFrame, fMapas, "map").commit();
                    //-------------------------
                    pintar();
                } else {
                    Log.e("RequestError", response.message());
                }
            }
            @Override
            public void onFailure(Call<ResponseContainerNoList<PropertiesResponse>> call, Throwable t) {
                Log.e("RequestError", "onFailure");
            }
        });
    }
    public void ocultar() {
        base.setVisibility(View.GONE);
    }
    public void mostrar(){
        base.setVisibility(View.VISIBLE);
    }
    public void pintar(){
        superior.setTitle(properti.getTitle());
        //titulo transparente hasta el scroll
        superior.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));
        this.mostrar();
       /* Glide
                .with(DetailPropertyActivity.this)
                .load(R.drawable.lista)
                .into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        superior.setBackground(resource);
                    }
                });*/

        if (properti.getCategoryId() != null) {
            if(properti.getCategoryId().getName().equals("Alquiler")){
                price.setText(String.valueOf(properti.getPrice())+" €/mes");
            }else if(properti.getCategoryId().getName().equals("Obra nueva") || properti.getCategoryId().getName().equals("Venta")){
                price.setText(String.valueOf(properti.getPrice())+" €");
            }else{
                price.setText(String.valueOf(properti.getPrice())+" €");
            }
            category.setText(properti.getCategoryId().getName());
        }
        else{
            price.setText(String.valueOf(properti.getPrice())+" €");
        }

        rooms.setText(String.valueOf(properti.getRooms())+" hab.");
        size.setText(String.valueOf(properti.getSize())+" m2");
        city.setText(properti.getCity());
        province.setText(properti.getProvince());
        zipcode.setText(properti.getZipcode());
        address.setText(properti.getAddress());
        description.setText(properti.getDescription());

        sliderLayout.setScrollTimeInSec(5); //set scroll delay in seconds :
        setSliderViews();
    }

    private void setSliderViews() {
        if(properti.getPhotos().length != 0){
            String p[] = properti.getPhotos();
            for (int i = 0; i < properti.getPhotos().length; i++) {
                DefaultSliderView sliderView = new DefaultSliderView(DetailPropertyActivity.this);
                switch (i) {
                    case 0:
                        sliderView.setImageUrl(p[i]);
                        break;
                    /*default:
                        sliderView.setImageUrl("https://st3.idealista.com/news/archivos/styles/news_detail/public/2018-11/casa_prefabricada.jpg?sv=pX_Hqy9d&itok=kCOtbqgQ");
                        break;*/
                }
                sliderView.setImageScaleType(ImageView.ScaleType.CENTER_CROP);
                sliderView.setDescription(properti.getTitle());
                //final int finalI = i;
                sliderView.setOnSliderClickListener(new SliderView.OnSliderClickListener() {
                    @Override
                    public void onSliderClick(SliderView sliderView) {
                        //click
                        //Toast.makeText(DetailPropertyActivity.this, "This is slider " + (finalI + 1), Toast.LENGTH_SHORT).show();
                    }
                });
                //at last add this view in your layout :
                sliderLayout.addSliderView(sliderView);
            }
        }
        else{
            Glide
                    .with(DetailPropertyActivity.this)
                    .load(R.drawable.lista)
                    .into(new SimpleTarget<Drawable>() {
                        @Override
                        public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                            superior.setBackground(resource);
                        }
                    });
        }
    }

}
