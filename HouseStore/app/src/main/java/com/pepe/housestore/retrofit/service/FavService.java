package com.pepe.housestore.retrofit.service;

import com.pepe.housestore.model.FavResponse;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface FavService {
    @POST("properties/fav/{id}")
    Call<FavResponse> addFav(@Path("id") String id);

    @DELETE("properties/fav/{id}")
    Call<FavResponse> deleteFav(@Path("id") String id);
}
