package com.pepe.housestore.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pepe.housestore.Adapter.PropertiesRecyclerViewAdapter;
import com.pepe.housestore.R;
import com.pepe.housestore.Listener.PropertiesInteractionListener;
import com.pepe.housestore.model.ResponseContainer;
import com.pepe.housestore.model.propiedad.PropertiesResponse;
import com.pepe.housestore.retrofit.Utils;
import com.pepe.housestore.retrofit.generator.ServiceGenerator;
import com.pepe.housestore.retrofit.generator.TipoAutenticacion;
import com.pepe.housestore.retrofit.service.PropertiesService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link PropertiesInteractionListener}
 * interface.
 */
public class PropertiesFavoriteFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;

    private PropertiesInteractionListener mListener;
    private PropertiesRecyclerViewAdapter adapter;

    private Context ctx;
    private List<PropertiesResponse> listaPropiedades;
    private RecyclerView recyclerView;

    private SwipeRefreshLayout swipe;


    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public PropertiesFavoriteFragment() {
        //this.fab = faborite;
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static PropertiesFavoriteFragment newInstance(int columnCount) {
        PropertiesFavoriteFragment fragment = new PropertiesFavoriteFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_properties_list, container, false);
        //recyclerView = (RecyclerView) view.findViewById(R.id.list_propeties);
        // Set the adapter
        if (view instanceof SwipeRefreshLayout) {
            Context context = view.getContext();

            recyclerView = (RecyclerView) view.findViewById(R.id.list_propeties);

            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            listaPropiedades = new ArrayList<>();
            this.getPropertiesFav(false);


            swipe = (SwipeRefreshLayout) view;
            swipe.setOnRefreshListener(
                    new SwipeRefreshLayout.OnRefreshListener() {
                        @Override
                        public void onRefresh() {
                            Log.i("TAG", "onRefresh called from SwipeRefreshLayout");

                            // This method performs the actual data-refresh operation.
                            // The method calls setRefreshing(false) when it's finished.
                            getPropertiesFav(true);
                            if (swipe.isRefreshing()) {
                                swipe.setRefreshing(false);
                            }
                        }
                    }
            );
        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.ctx = context;

        if (context instanceof PropertiesInteractionListener) {
            mListener = (PropertiesInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement PropertiesInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
   /* public interface PropertiesInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(PropertiesResponse item);
    }*/
    public void getPropertiesFav(final boolean update){
        PropertiesService service;
        service = ServiceGenerator.createService(PropertiesService.class, Utils.getToken(ctx), TipoAutenticacion.JWT);
        Call<ResponseContainer<PropertiesResponse>> call = service.listaPropertiesFav();
        call.enqueue(new Callback<ResponseContainer<PropertiesResponse>>() {
            @Override
            public void onResponse(Call<ResponseContainer<PropertiesResponse>> call, Response<ResponseContainer<PropertiesResponse>> response) {
                if (response.isSuccessful()) {
                    // error
                    listaPropiedades = response.body().getRows();
                    int yesFavoriteFragment = 1;
                    adapter = new PropertiesRecyclerViewAdapter(
                            ctx,
                            listaPropiedades,
                            mListener,
                            yesFavoriteFragment
                    );

                    recyclerView.setAdapter(adapter);

                } else {
                    Log.e("RequestError", response.message());
                }
            }
            @Override
            public void onFailure(Call<ResponseContainer<PropertiesResponse>> call, Throwable t) {
                //View r = setContentView(R.layout.activity_main);
                Log.e("RequestError", "onFailure");
                //Snackbar.make(getActivity().findViewById(android.R.id.content),"Complete los campos", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }
        });
    }
    @Override
    public void onResume() {
        super.onResume();
        this.getPropertiesFav(true);

    }
}
