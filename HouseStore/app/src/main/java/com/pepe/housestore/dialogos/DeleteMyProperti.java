package com.pepe.housestore.dialogos;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;

import com.pepe.housestore.Listener.DialogListener;
import com.pepe.housestore.model.Propiedad;
import com.pepe.housestore.model.propiedad.MyPropertiesResponse;

public class DeleteMyProperti extends DialogFragment implements DialogListener {
    private Context ctx;
    private DialogListener mListener;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle("Borar Propiedad")
                .setMessage("Estas apunto de borrar una propiedad")
                .setPositiveButton("Borrar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mListener = (DialogListener) ctx;
                        mListener.deleteProperti();
                        /*RetrofitMyProperties r = new RetrofitMyProperties(ctx);
                        r.deleteProperti();*/
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        return builder.create();
    }

    @Override
    public void deleteProperti() {

    }

    @Override
    public void addProperti(Propiedad p) {

    }

    @Override
    public void editProperti(Propiedad p, String id) {

    }


    public void contexto(Context ctx){
        this.ctx = ctx;
    }

}
