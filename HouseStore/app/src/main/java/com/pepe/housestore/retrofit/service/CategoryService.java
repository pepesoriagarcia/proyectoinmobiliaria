package com.pepe.housestore.retrofit.service;


import com.pepe.housestore.model.CategoryResponse;
import com.pepe.housestore.model.ResponseContainer;
import com.pepe.housestore.model.propiedad.Category;

import retrofit2.Call;
import retrofit2.http.GET;


public interface CategoryService {
    @GET("/categories")
    Call<ResponseContainer<CategoryResponse>> getCategory();
}
