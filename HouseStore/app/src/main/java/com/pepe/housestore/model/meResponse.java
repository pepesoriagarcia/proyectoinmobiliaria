package com.pepe.housestore.model;

public class meResponse {
    private String id;
    private String name;
    private String picture;
    private String email;
    private String createdAt;
    private String favs[];

    public meResponse(String id, String name, String picture, String email, String createdAt, String[] favs) {
        this.id = id;
        this.name = name;
        this.picture = picture;
        this.email = email;
        this.createdAt = createdAt;
        this.favs = favs;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String[] getFavs() {
        return favs;
    }

    public void setFavs(String[] favs) {
        this.favs = favs;
    }
}
